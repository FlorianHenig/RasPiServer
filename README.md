# How To
1. Flash Raspberry Pi OS Lite (x64) to a sd card
1. SSH into server
1. Install git `sudo apt install git`
1. Clone Repository e.g. to $HOME: `git clone https://git.disroot.org/FlorianHenig/RasPiServer.git --depth 1 --branch=master`
1. Make *install.sh* executable: `sudo chmod +x install.sh`
1. Run *install.sh* and follow on screen instructions: `./install.sh`

Copy-Paste following inside your terminal
```shell
cd ~
sudo apt install git
git clone https://git.disroot.org/FlorianHenig/RasPiServer.git --depth 1 --branch=master
cd RasPiServer/
sudo chmod +x ./install.sh
./install.sh
```