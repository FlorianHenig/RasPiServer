#!/bin/bash
# Saving the scripts directory for further access
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SCRIPT_NAME="${0##*/}"
SERVICE="${SCRIPT_NAME%.*}"
echo "Installing $SERVICE (Docker)"
sudo docker compose -f $SCRIPT_DIR/../../docker/compose/$SERVICE/docker-compose.yml up -d
