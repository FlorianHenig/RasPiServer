#!/bin/bash
# Saving the scripts directory for further access
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SCRIPT_NAME="${0##*/}"
SERVICE="${SCRIPT_NAME%.*}"
echo "Installing $SERVICE (Docker)"
# Download docker compose file from the guardian projects repo
sudo mkdir --parents "$SCRIPT_DIR/../../docker/compose/$SERVICE/"
sudo wget --directory-prefix="$SCRIPT_DIR/../../docker/compose/$SERVICE/" https://gitlab.torproject.org/tpo/anti-censorship/docker-snowflake-proxy/-/raw/main/docker-compose.yml
sudo docker compose -f $SCRIPT_DIR/../../docker/compose/$SERVICE/docker-compose.yml up -d