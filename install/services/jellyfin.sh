#!/bin/bash
sudo mkdir /mnt/ramdisk_transcodes/ &>/dev/null # create mountpoint for ramdisk if nonexistend
sudo chown 1000:1000 /mnt/ramdisk_transcodes/ &>/dev/null
sudo mount -t tmpfs -o size=1G tmpfs /mnt/ramdisk_transcodes/ #Is called inside start_services.sh script but needs to be called here also for setup and first run
sudo mkdir /mnt/media_hdd/media/shows &>/dev/null # create dir for shows if nonexistend (must match dirs inside docker compose file)
sudo mkdir /mnt/media_hdd/media/movies &>/dev/null # create dir for movies if nonexistend (must match dirs inside docker compose file)
# fixing some permission in case they got messed up
sudo chown --recursive 1000:1000 /mnt/media_hdd/media/shows/ &>/dev/null
sudo chown --recursive 1000:1000 /mnt/media_hdd/media/movies/ &>/dev/null
sudo chown --recursive 1000:1000 /mnt/media_hdd/media/jellyfin/ &>/dev/null
# Saving the scripts directory for further access
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SCRIPT_NAME="${0##*/}"
SERVICE="${SCRIPT_NAME%.*}"
echo "Installing $SERVICE (Docker)"
sudo docker compose -f $SCRIPT_DIR/../../docker/compose/$SERVICE/docker-compose.yml up -d
