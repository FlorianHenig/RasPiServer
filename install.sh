#!/bin/bash
# Saving the scripts directory for further access
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo '######################################'
echo '# Setting up your RaspberryPi server #'
echo '######################################'
#####################################
# Setting up dockers apt repository #
#####################################
echo "Setting up dockers apt repository"
# see https://docs.docker.com/engine/install/debian/#install-using-the-repository
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
########
# DONE #
########

#################################
# Installing neccessary sofware #
#################################
# Cryptsetup for encrypted drives
sudo apt install -y cryptsetup
# tmux (a terminal multiplexer) for keeping remote terminal sessions open easily
sudo apt install -y tmux
# Docker for all the services running on the server
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
########
# DONE #
########

#################################################
# Adding Ramdisk to system for future LUKS-Keys #
#################################################
sudo mkdir /mnt/keys
sudo echo "tmpfs           /mnt/keys       tmpfs   nodev,nosuid,size=1M    0     0" | sudo tee --append /etc/fstab >/dev/null

# Mounting the ramdisk this time manually for the installation process
sudo mount -t tmpfs -o size=1m none /mnt/keys

echo -n "[ACTION] Put Keyfiles in \"/mnt/keys\" and press ENTER to proceed "
read

#######################################
# Setting up the encrypted ext drives #
#######################################
echo "=> Setting up the encrypted ext drives"
sudo lsblk -o +name,mountpoint,uuid
echo -n "[ACTION] UUID of nextcloud_hdd: "
read uuid_nextcloud_hdd

echo -n "[ACTION] UUID of nextcloud_backup_hdd: "
read uuid_nextcloud_backup_hdd

echo -n "[ACTION] UUID of media_hdd: "
read uuid_media_hdd

sudo echo "export UUID_NEXTCLOUD_HDD=\"$uuid_nextcloud_hdd\"" | sudo tee --append ~/.profile > /dev/null
sudo echo "export UUID_NEXTCLOUD_BACKUP_HDD=\"$uuid_nextcloud_backup_hdd\"" | sudo tee --append ~/.profile > /dev/null
sudo echo "export UUID_MEDIA_HDD=\"$uuid_media_hdd\"" | sudo tee --append ~/.profile > /dev/null
source ~/.profile

DEVICE="nextcloud_hdd"
UUID=UUID_${DEVICE^^}
UUID="${!UUID}"
mount_point="/mnt/$DEVICE"
keyfile="/mnt/keys/$DEVICE.key"
echo "... Mounting $DEVICE"
sudo mkdir $mount_point
sudo cryptsetup --key-file=$keyfile open /dev/disk/by-uuid/$UUID $DEVICE && sudo rm -f $keyfile
sudo mount /dev/mapper/$DEVICE $mount_point

DEVICE="nextcloud_backup_hdd"
UUID=UUID_${DEVICE^^}
UUID="${!UUID}"
mount_point="/mnt/$DEVICE"
keyfile="/mnt/keys/$DEVICE.key"
echo "... Mounting $DEVICE"
sudo mkdir $mount_point
sudo cryptsetup --key-file=$keyfile open /dev/disk/by-uuid/$UUID $DEVICE && sudo rm -f $keyfile
sudo mount /dev/mapper/$DEVICE $mount_point

DEVICE="media_hdd"
UUID=UUID_${DEVICE^^}
UUID="${!UUID}"
mount_point="/mnt/$DEVICE"
keyfile="/mnt/keys/$DEVICE.key"
echo "... Mounting $DEVICE"
sudo mkdir $mount_point
sudo cryptsetup --key-file=$keyfile open /dev/disk/by-uuid/$UUID $DEVICE && sudo rm -f $keyfile
sudo mount /dev/mapper/$DEVICE $mount_point
########
# DONE #
########

######################
# Installing services #
######################
echo "=> Installing services:"
sudo chmod +x $SCRIPT_DIR/install/services/*.sh # make all scripts executable
scripts=$SCRIPT_DIR/install/services/*.sh
# Iteratring over all scripts and executing them
for script in $scripts
do
  echo -n "..."
  $script # Execute installer script
done
########
# DONE #
########

sudo chmod +x $SCRIPT_DIR/start_services.sh
echo "Info: For starting services in the future use startup script located at: $SCRIPT_DIR/start_services.sh"
echo '######################################'
echo '#            Setup done              #'
echo '######################################'
