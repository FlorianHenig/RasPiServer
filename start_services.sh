#!/bin/bash
echo -n "Put keyfiles in \"/mnt/keys\" and press ENTER to proceed "
read

DEVICE="nextcloud_hdd"
UUID=UUID_${DEVICE^^}
UUID="${!UUID}"
mount_point="/mnt/$DEVICE"
keyfile="/mnt/keys/$DEVICE.key"
echo "#### Opening $DEVICE"
sudo cryptsetup --key-file=$keyfile open /dev/disk/by-uuid/$UUID $DEVICE && sudo rm -f $keyfile
# Abort startup process, if previous command failes.
RES=$(echo $?)
if [[ 1 -eq $res ]]; then
    exit 1
fi
sudo mount /dev/mapper/$DEVICE $mount_point
# Abort startup process, if previous command failes.
RES=$(echo $?)
if [[ 1 -eq $res ]]; then
    exit 2
fi

DEVICE="nextcloud_backup_hdd"
UUID=UUID_${DEVICE^^}
UUID="${!UUID}"
mount_point="/mnt/$DEVICE"
keyfile="/mnt/keys/$DEVICE.key"
echo "#### Opening $DEVICE"
sudo cryptsetup --key-file=$keyfile open /dev/disk/by-uuid/$UUID $DEVICE && sudo rm -f $keyfile
# Abort startup process, if previous command failes.
RES=$(echo $?)
if [[ 1 -eq $res ]]; then
    exit 3
fi
sudo mount /dev/mapper/$DEVICE $mount_point
# Abort startup process, if previous command failes.
RES=$(echo $?)
if [[ 1 -eq $res ]]; then
    exit 4
fi

DEVICE="media_hdd"
UUID=UUID_${DEVICE^^}
UUID="${!UUID}"
mount_point="/mnt/$DEVICE"
keyfile="/mnt/keys/$DEVICE.key"
echo "#### Opening $DEVICE"
sudo cryptsetup --key-file=$keyfile open /dev/disk/by-uuid/$UUID $DEVICE && sudo rm -f $keyfile
# Abort startup process, if previous command failes.
RES=$(echo $?)
if [[ 1 -eq $res ]]; then
    exit 5
fi
sudo mount /dev/mapper/$DEVICE $mount_point
# Abort startup process, if previous command failes.
RES=$(echo $?)
if [[ 1 -eq $res ]]; then
    exit 6
fi

# Just to be safe ;)
sudo rm -rf /mnt/keys/* &>/dev/null
echo "#### All keys flushed from memory :)"

echo "#### Starting Nextcloud"
sudo docker start nextcloud-aio-mastercontainer
IPADDRESSES=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
echo "#### Please use Web UI to start Nextcloud"
echo "(one of the links below will leed you to the Web UI)"
for address in $IPADDRESSES; do
    echo "https://$address:8080/"
done

echo "#### Mounting Ramdisk for video transcoding"
sudo mount -t tmpfs -o size=1G tmpfs /mnt/ramdisk_transcodes/

echo "#### Starting Jellyfin"
sudo docker start jellyfin

echo "#### Staring Pyload-NG"
sudo docker start pyload-ng